/*
 * Dependencies.kt
 * PhunApp
 *
 * Created by lalo on 12/5/21 22:37
 * Copyright (c) 2021 . All rights reserved.
 */

object Sdk {
    const val MIN_SDK_VERSION = 26
    const val TARGET_SDK_VERSION = 30
    const val COMPILE_SDK_VERSION = 30
}

object Build {
    /* */
    const val BUILD_TOOL_VERSION = "30.0.3"
}

object AndroidTools {
    /* */
    const val CLASS_PATH_ANDROID_TOOLS_GRADLE =
        "com.android.tools.build:gradle:${Versions.CLASS_PATH_ANDROID_TOOLS_GRADLE_VERSION}"
}

object KotlinVersion {
    /* */
    const val KOTLIN_VERSION = "1.4.32"
}

object Kotlin {
    /* */
    const val GRADLE_PLUGIN =
        "org.jetbrains.kotlin:kotlin-gradle-plugin:${KotlinVersion.KOTLIN_VERSION}"
}

object Versions {
    /** CLASSPATH */
    const val CLASS_PATH_ANDROID_TOOLS_GRADLE_VERSION = "4.1.0"
    /** ANDROID X */
    const val CONSTRAINT_LAYOUT_VERSION = "2.0.4"
    const val CORE_KTX_VERSION = "1.3.2"
    const val APPCOMPAT_VERSION = "1.2.0"
    const val NAVIGATION_VERSION = "2.3.3"
    const val LEGACY_SUPPORT_VERSION = "1.0.0"
    const val RECYCLERVIEW_VERSION = "1.2.0-alpha03"

    /** DEBUG */
    const val TIMBER_VERSION = "4.7.1"

    /** RETROFIT */
    const val RETROFIT_VERSION = "2.9.0"
    const val RETROFIT_OKHTTP_VERSION = "4.8.0"

    /** MOSHI */
    const val MOSHI_VERSION = "1.9.3"

    /** GOOGLE */
    const val MATERIAL_COMPONENTS_VERSION = "1.2.0-alpha06"

    /** KOIN */
    const val KOIN_VERSION = "2.2.2"

    /** LIFECYCLE */
    const val LIFECYCLE_VERSION = "2.2.0"

    /** COROUTINES */
    const val KOTLIN_COROUTINES_VERSION = "1.3.9"
    const val COROUTINES_PLAY_SERVICES_VERSION = "1.1.1"

    /** COIL */
    const val COIL_VERSION = "0.11.0"

    /** APP INTRO */
    const val APP_INTRO = "6.0.0"

    /** ANDROID TESTING LIBS */
    const val ANDROIDX_TEST_VERSION = "1.2.0"
    const val ANDROIDX_TEST_EXT_VERSION = "1.1.2"
    const val ESPRESSO_CORE_VERSION = "3.3.0"
    const val JUNIT_VERSION = "4.13.1"
    const val MOCKK = "1.10.5"
    const val COROUTINES_TEST = "1.4.2"
    const val MOCK_WEB_SERVER = "4.9.0"
    const val FAKER_VERSION = "1.0.2"

    /* */
    const val HTML_TEXTVIEW = "4.0"
}

object BuildPluginsVersion {
    const val AGP = "4.1.0"
    const val KOTLIN = "1.4.10"
    const val VERSIONS_PLUGIN = "0.28.0"
}

object TestingLib {
    const val JUNIT = "junit:junit:${Versions.JUNIT_VERSION}"
}

object AndroidTestingLib {
    const val ANDROIDX_TEST_RULES = "androidx.test:rules:${Versions.ANDROIDX_TEST_VERSION}"
    const val ANDROIDX_TEST_RUNNER = "androidx.test:runner:${Versions.ANDROIDX_TEST_VERSION}"
    const val ANDROIDX_TEST_EXT_JUNIT =
        "androidx.test.ext:junit:${Versions.ANDROIDX_TEST_EXT_VERSION}"
    const val ESPRESSO_CORE =
        "androidx.test.espresso:espresso-core:${Versions.ESPRESSO_CORE_VERSION}"
    const val MOCKK = "io.mockk:mockk:${Versions.MOCKK}"
    const val COROUTINES_TEST =
        "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.COROUTINES_TEST}"
    const val MOCK_WEB_SERVER = "com.squareup.okhttp3:mockwebserver:${Versions.MOCK_WEB_SERVER}"
    const val FAKER = "com.github.javafaker:javafaker:${Versions.FAKER_VERSION}"
}

object Dependencies {
    /** KOTLIN JETBREAINS */
    const val JET_BRAINS_KOTLIN =
        "org.jetbrains.kotlin:kotlin-stdlib:${KotlinVersion.KOTLIN_VERSION}"

    /** ANDROID X */
    const val CORE_KTX = "androidx.core:core-ktx:${Versions.CORE_KTX_VERSION}"
    const val APP_COMPAT = "androidx.appcompat:appcompat:${Versions.APPCOMPAT_VERSION}"
    const val CONSTRAINT_LAYOUT =
        "androidx.constraintlayout:constraintlayout:${Versions.CONSTRAINT_LAYOUT_VERSION}"
    const val NAVIGATION_FRAGMENT =
        "androidx.navigation:navigation-fragment-ktx:${Versions.NAVIGATION_VERSION}"
    const val NAVIGATION_UI = "androidx.navigation:navigation-ui-ktx:${Versions.NAVIGATION_VERSION}"
    const val NAVIGATION_SAFE_ARGS =
        "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.NAVIGATION_VERSION}"
    const val LEGACY_SUPPORT =
        "androidx.legacy:legacy-support-v4:${Versions.LEGACY_SUPPORT_VERSION}"
    const val RECYCLERVIEW = "androidx.recyclerview:recyclerview:${Versions.RECYCLERVIEW_VERSION}"

    /** DEBUG */
    const val TIMBER = "com.jakewharton.timber:timber:${Versions.TIMBER_VERSION}"

    /** RETROFIT */
    const val RETROFIT = "com.squareup.retrofit2:retrofit:${Versions.RETROFIT_VERSION}"
    const val RETROFIT_OKHTTP =
        "com.squareup.okhttp3:logging-interceptor:${Versions.RETROFIT_OKHTTP_VERSION}"

    /** MOSHI */
    const val MOSHI_CODEGEN = "com.squareup.moshi:moshi-kotlin-codegen:${Versions.MOSHI_VERSION}"
    const val MOSHI = "com.squareup.moshi:moshi:${Versions.MOSHI_VERSION}"
    const val MOSHI_CONVERTER =
        "com.squareup.retrofit2:converter-moshi:${Versions.RETROFIT_VERSION}"
    const val MOSHI_REFLECTION = "com.squareup.moshi:moshi-kotlin:${Versions.MOSHI_VERSION}"

    /** GOOGLE */
    const val MATERIAL_COMPONENTS =
        "com.google.android.material:material:${Versions.MATERIAL_COMPONENTS_VERSION}"

    /** KOIN */
    const val KOIN_ANDROID = "org.koin:koin-android:${Versions.KOIN_VERSION}"
    const val KOIN_VIEWMODEL = "org.koin:koin-androidx-viewmodel:${Versions.KOIN_VERSION}"

    /** LIFECYCLE */
    const val LIFECYCLE_LIVE_DATA =
        "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.LIFECYCLE_VERSION}"
    const val LIFECYCLE_EXTENSIONS =
        "androidx.lifecycle:lifecycle-extensions:${Versions.LIFECYCLE_VERSION}"
    const val LIFECYCLE_VIEW_MODEL =
        "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.LIFECYCLE_VERSION}"

    /** COROUTINES */
    const val COROUTINES_CORE =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.KOTLIN_COROUTINES_VERSION}"
    const val COROUTINES_ANDROID =
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.KOTLIN_COROUTINES_VERSION}"
    const val COROUTINES_PLAY_SERVICE =
        "org.jetbrains.kotlinx:kotlinx-coroutines-play-services:${Versions.COROUTINES_PLAY_SERVICES_VERSION}"

    /** COIL */
    const val COIL = "io.coil-kt:coil:${Versions.COIL_VERSION}"

    /** APP INTRO */
    const val APP_INTRO = "com.github.AppIntro:AppIntro:${Versions.APP_INTRO}"
    const val HTML_TEXTVIEW = "org.sufficientlysecure:html-textview:${Versions.HTML_TEXTVIEW}"

    const val MATERIAL_STAR_BAR = "me.zhanghai.android.materialratingbar:library:1.4.0"
}

object Module {
    /** DOMAIN */
    const val DOMAIN = ":domain"
    /** NETWORK */
    const val NETWORK = ":network"
    /** NETWORK */
    const val DATA_SOURCE = ":dataSource"
    /** MOVIE */
    const val MOVIES = ":movies"
}

/* */
object KotlinX {

    /* */
    private object Version {

        /* */
        const val KOTLINX = "1.2.1"

        /* */
        const val KOTLINX_CONVERTER = "0.8.0"

    }

    /* */
    const val KOTLINX = "org.jetbrains.kotlinx:kotlinx-serialization-json:${Version.KOTLINX}"

    /* */
    const val KOTLINX_CONVERTER =
        "com.jakewharton.retrofit:retrofit2-kotlinx-serialization-converter:${Version.KOTLINX_CONVERTER}"

}

/* */
object Room {

    /* */
    private object Version {

        /* */
        const val ROOM = "2.2.6"

    }

    /* */
    const val RUNTIME = "androidx.room:room-runtime:${Version.ROOM}"

    /* */
    const val COMPILER = "androidx.room:room-compiler:${Version.ROOM}"

    /* */
    const val KTX_EXTENSIONS = "androidx.room:room-ktx:${Version.ROOM}"

}

