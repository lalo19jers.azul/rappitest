package com.mx.movies.data.remote

import com.mx.movies.data.dataSource.remote.MovieApiService
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.InputStreamReader

class MovieApiServiceTest {
    /* */
    private val mockWebServer = MockWebServer()

    /* */
    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    }

    /* */
    private val apiService by lazy {
        retrofit.create(MovieApiService::class.java)
    }

    /* */
    private lateinit var getAllMoviesAsJson: String

    /** */
    @Before
    fun setUp() {
        val readerAllMovies =
            InputStreamReader(this.javaClass.classLoader?.getResourceAsStream("all_movies.json"))

        getAllMoviesAsJson = readerAllMovies.readText()

        readerAllMovies.close()
    }


    @Test
    fun `get all movies response returns success deserialization from example json`() {
        mockWebServer.enqueue(
            MockResponse()
                .setBody(getAllMoviesAsJson)
                .setResponseCode(200)
        )

        val result = runBlocking {
            apiService.getAllMovies(
                listId = 1, apiKey = "", ""
            )
        }

        Assert.assertNotNull(result.body())
    }


    @After
    fun teardown() {
        mockWebServer.shutdown()
    }
}