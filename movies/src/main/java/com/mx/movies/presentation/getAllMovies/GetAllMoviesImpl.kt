package com.mx.movies.presentation.getAllMovies

import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import com.mx.domain.Status
import com.mx.domain.onFailure
import com.mx.domain.onRight
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesParams
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow

class GetAllMoviesImpl(
    private val getAllMoviesUseCase: GetAllMoviesUseCase
) : GetAllMovies {

    /** */
    override fun getAllMoviesAsLiveData(
        params: GetAllMoviesParams
    ): LiveData<GetAllMoviesStatus> = flow<GetAllMoviesStatus> {
        emit(Status.Loading())
        getAllMoviesUseCase.run(params)
            .onFailure { emit(Status.Error(it)) }
            .onRight { emit(Status.Done(it)) }
    }.asLiveData(Dispatchers.IO)

}