package com.mx.movies.presentation.getMovieDetail

import androidx.lifecycle.LiveData
import com.mx.domain.Status
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailFailure
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailParams
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailResponse

/** */
typealias GetMovieDetailStatus =
        Status<GetMovieDetailFailure, GetMovieDetailResponse>

interface GetMovieDetail  {

    /** */
    fun getMovieDetailAsLiveData(
        params: GetMovieDetailParams
    ): LiveData<GetMovieDetailStatus>
}