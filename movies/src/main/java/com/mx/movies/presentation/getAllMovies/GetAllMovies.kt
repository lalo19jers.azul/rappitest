package com.mx.movies.presentation.getAllMovies

import androidx.lifecycle.LiveData
import com.mx.domain.Status
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesFailure
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesParams
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesResponse

/** */
typealias GetAllMoviesStatus =
        Status<GetAllMoviesFailure, GetAllMoviesResponse>

interface GetAllMovies {

    /** */
    fun getAllMoviesAsLiveData(
        params: GetAllMoviesParams
    ): LiveData<GetAllMoviesStatus>

}