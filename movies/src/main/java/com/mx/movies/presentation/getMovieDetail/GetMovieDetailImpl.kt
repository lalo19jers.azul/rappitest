package com.mx.movies.presentation.getMovieDetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import com.mx.domain.Status
import com.mx.domain.onFailure
import com.mx.domain.onRight
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailParams
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow

class GetMovieDetailImpl(
    private val getMovieDetailUseCase: GetMovieDetailUseCase
): GetMovieDetail {

    /** */
    override fun getMovieDetailAsLiveData(
        params: GetMovieDetailParams
    ): LiveData<GetMovieDetailStatus> = flow<GetMovieDetailStatus> {
        emit(Status.Loading())
        getMovieDetailUseCase.run(params)
            .onFailure { emit(Status.Error(it)) }
            .onRight {  emit(Status.Done(it))  }
    }.asLiveData(Dispatchers.IO)
}