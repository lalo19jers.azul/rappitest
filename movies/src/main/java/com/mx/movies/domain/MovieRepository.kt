package com.mx.movies.domain

import com.mx.datasource.local.entity.MovieDb
import com.mx.datasource.local.entity.MovieDetailDb
import com.mx.domain.Either
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesFailure
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesParams
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesResponse
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailFailure
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailParams
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailResponse

interface MovieRepository {

    /** */
    suspend fun getMovieInfo(
        params: GetAllMoviesParams
    ): Either<GetAllMoviesFailure, GetAllMoviesResponse>

    /** */
    suspend fun getMovieDetail(
        params: GetMovieDetailParams
    ): Either<GetMovieDetailFailure, GetMovieDetailResponse>

    /** */
    suspend fun getMoviesFromDb(): Either<GetAllMoviesFailure, List<MovieDb>>

    /** */
    suspend fun getMovieDetailFromDb(): Either<GetMovieDetailFailure, MovieDetailDb>
}