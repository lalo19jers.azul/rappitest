package com.mx.movies.domain.useCase.getMovieDetail

import com.mx.domain.Either
import com.mx.domain.UseCase
import com.mx.movies.domain.MovieRepository

class GetMovieDetailUseCase(
    private val repository: MovieRepository
): UseCase<GetMovieDetailResponse, GetMovieDetailParams, GetMovieDetailFailure>() {

    /** */
    override suspend fun run(params: GetMovieDetailParams
    ): Either<GetMovieDetailFailure, GetMovieDetailResponse> =
        repository.getMovieDetail(params = params)

}