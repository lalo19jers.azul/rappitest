package com.mx.movies.domain.useCase.getAllMovies

data class GetAllMoviesParams(
    val page: Int,
    val language: String,
    val apiKey: String
)