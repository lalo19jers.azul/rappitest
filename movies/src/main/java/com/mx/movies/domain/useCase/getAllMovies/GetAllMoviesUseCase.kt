package com.mx.movies.domain.useCase.getAllMovies

import com.mx.domain.Either
import com.mx.domain.UseCase
import com.mx.movies.domain.MovieRepository

class GetAllMoviesUseCase(
    private val repository: MovieRepository
): UseCase<GetAllMoviesResponse, GetAllMoviesParams, GetAllMoviesFailure>() {

    /** */
    override suspend fun run(params: GetAllMoviesParams
    ): Either<GetAllMoviesFailure, GetAllMoviesResponse> =
        repository.getMovieInfo(params = params)

}