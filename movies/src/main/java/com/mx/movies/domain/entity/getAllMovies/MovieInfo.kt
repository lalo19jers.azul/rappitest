package com.mx.movies.domain.entity.getAllMovies

data class MovieInfo(
    val description: String,
    val name: String,
    val movies: List<Movie>
)
