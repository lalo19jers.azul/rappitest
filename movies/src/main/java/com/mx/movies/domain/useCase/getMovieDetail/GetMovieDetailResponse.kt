package com.mx.movies.domain.useCase.getMovieDetail

import com.mx.movies.domain.entity.getMovieDetail.MovieDetailInfo

data class GetMovieDetailResponse(
    val movieDetailInfo: MovieDetailInfo
)