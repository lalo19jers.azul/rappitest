package com.mx.movies.domain.useCase.getMovieDetail

data class GetMovieDetailParams(
    val movieId: Int,
    val language: String,
    val apiKey: String
)