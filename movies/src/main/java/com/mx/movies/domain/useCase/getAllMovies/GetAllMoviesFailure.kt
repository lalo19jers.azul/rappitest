package com.mx.movies.domain.useCase.getAllMovies

import com.mx.datasource.failureManage.HttpFailure
import com.mx.domain.Failure

sealed class GetAllMoviesFailure: Failure.FeatureFailure() {
    /* */
    object NetworkConnectionFailure : GetAllMoviesFailure()

    /* */
    object NoMoviesFound : GetAllMoviesFailure()

    /* */
    object NullPointerException: GetAllMoviesFailure()

    /* */
    data class JsonDataDeserializationFailure(val message: String?) : GetAllMoviesFailure()

    /* */
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : GetAllMoviesFailure(), HttpFailure

    /* */
    data class UnknownFailure(val message: String?) : GetAllMoviesFailure()

    /** */
    companion object {
        internal fun fromFeatureFailure(failure: Failure) = when (failure) {
            is GetAllMoviesFailure -> failure
            else -> UnknownFailure(failure.javaClass.simpleName)
        }
    }
}