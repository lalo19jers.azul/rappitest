package com.mx.movies.domain.entity.getAllMovies


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
 data class Movie(
    val backdropPath: String,
    val title: String,
    val id: Int,
    val overview: String,
    val posterPath: String,
    val voteAverage: Float,
    val video: Boolean
): Parcelable
