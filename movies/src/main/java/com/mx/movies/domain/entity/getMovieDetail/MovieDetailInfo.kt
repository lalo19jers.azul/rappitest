package com.mx.movies.domain.entity.getMovieDetail


data class MovieDetailInfo(
    val genres: List<Genre>,
    val tagline: String,
    val title: String,
    val id: Int,
    val runtime: Int,
    val overview: String,
    val posterPath: String,
    val voteAverage: Float,
)
