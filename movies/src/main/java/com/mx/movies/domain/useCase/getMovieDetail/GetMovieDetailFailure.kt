package com.mx.movies.domain.useCase.getMovieDetail

import com.mx.datasource.failureManage.HttpFailure
import com.mx.domain.Failure
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesFailure

sealed class GetMovieDetailFailure: Failure.FeatureFailure() {
    /* */
    object NetworkConnectionFailure : GetMovieDetailFailure()

    /* */
    object NoMovieFound : GetMovieDetailFailure()

    /* */
    object NullPointerException: GetMovieDetailFailure()

    /* */
    data class JsonDataDeserializationFailure(val message: String?) : GetMovieDetailFailure()

    /* */
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : GetMovieDetailFailure(), HttpFailure

    /* */
    data class UnknownFailure(val message: String?) : GetMovieDetailFailure()

    /** */
    companion object {
        internal fun fromFeatureFailure(failure: Failure) = when (failure) {
            is GetAllMoviesFailure -> failure
            else -> UnknownFailure(failure.javaClass.simpleName)
        }
    }
}