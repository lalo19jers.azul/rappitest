package com.mx.movies.domain.useCase.getAllMovies

import com.mx.movies.domain.entity.getAllMovies.Movie

data class GetAllMoviesResponse(
    val movies: List<Movie>
)