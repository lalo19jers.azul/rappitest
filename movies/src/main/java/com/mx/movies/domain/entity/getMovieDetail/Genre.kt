package com.mx.movies.domain.entity.getMovieDetail

data class Genre(
    val name: String
)
