package com.mx.movies.data.dataSource.remote.model.dto.getMovieDetail

import com.mx.movies.domain.entity.getMovieDetail.Genre
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GenreDto(
    @Json(name = "name") val name: String,
) {

    /** */
    fun toGenre() = Genre(name = name)
}
