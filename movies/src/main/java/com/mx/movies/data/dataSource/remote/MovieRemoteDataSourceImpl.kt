package com.mx.movies.data.dataSource.remote

import com.mx.datasource.local.AppDatabase
import com.mx.datasource.remote.model.retrofitApiCall
import com.mx.domain.Either
import com.mx.movies.data.dataSource.MovieRemoteDataSource
import com.mx.movies.data.dataSource.remote.failure.toMovieDetailFailure
import com.mx.movies.data.dataSource.remote.failure.toMovieFailure
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesFailure
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesParams
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesResponse
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailFailure
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailParams
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailResponse
import retrofit2.HttpException

internal class MovieRemoteDataSourceImpl(
    private val db: AppDatabase,
    private val apiService: MovieApiService
) : MovieRemoteDataSource{

    /** */
    override suspend fun getMovieInfo(
        params: GetAllMoviesParams
    ): Either<GetAllMoviesFailure, GetAllMoviesResponse> =
        try {
            retrofitApiCall {
                apiService.getAllMovies(
                    listId = params.page,
                    apiKey = params.apiKey,
                    language = params.language
                )
            }.let { movieResponse ->
                for (movie in movieResponse.movies) {
                    db.movieDao.insert(
                        movie = movie.toMovieDb()
                    )
                }
                Either.Right(GetAllMoviesResponse(movieResponse.movies.map { it.toMovie() }))
            }
        } catch (e: Exception) {
            val failure = when (e) {
                is HttpException -> e.toMovieFailure()
                else -> GetAllMoviesFailure.UnknownFailure(e.message)
            }
            Either.Left(failure)
        }

    /** */
    override suspend fun getMovieDetail(
        params: GetMovieDetailParams
    ): Either<GetMovieDetailFailure, GetMovieDetailResponse> =
        try {
            retrofitApiCall {
                apiService.getMovieDetail(
                    movieId = params.movieId,
                    apiKey = params.apiKey,
                    language = params.language
                )
            }.let {
                db.movieDetailDao.insert(it.toMovieDetail())
                Either.Right(GetMovieDetailResponse(it.toMovieDetailInfo()))
            }
        } catch (e: Exception) {
            val failure = when (e) {
                is HttpException -> e.toMovieDetailFailure()
                else -> GetMovieDetailFailure.UnknownFailure(e.message)
            }
            Either.Left(failure)
        }
}