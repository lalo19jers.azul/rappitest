package com.mx.movies.data.dataSource.remote.model.dto.getMovieDetail

import com.mx.datasource.local.entity.dbMovieDetail
import com.mx.movies.domain.entity.getMovieDetail.MovieDetailInfo
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieDetailInfoDto(
    @Json(name = "genres") val genres: List<GenreDto>,
    @Json(name = "tagline") val tagline: String,
    @Json(name = "title") val title: String,
    @Json(name = "id") val id: Int,
    @Json(name = "runtime") val runtime: Int?,
    @Json(name = "overview") val overview: String,
    @Json(name = "poster_path") val posterPath: String,
    @Json(name = "vote_average") val voteAverage: Float,
) {

    /** */
    fun toMovieDetailInfo() : MovieDetailInfo =
        MovieDetailInfo(
            genres = genres.map { it.toGenre() },
            tagline = tagline,
            title = title,
            id = id,
            runtime = runtime ?: 0,
            overview = overview,
            posterPath = posterPath,
            voteAverage = voteAverage
        )

    /** */
    fun toMovieDetail(): dbMovieDetail =
        dbMovieDetail(
            id = id,
            tagline = tagline,
            title = title,
            runtime = runtime ?: 0,
            overview = overview,
            posterPath = posterPath,
            voteAverage = voteAverage
        )
}
