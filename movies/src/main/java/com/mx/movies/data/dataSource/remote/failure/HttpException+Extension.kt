package com.mx.movies.data.dataSource.remote.failure

import com.mx.datasource.remote.model.HttpErrorCode
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesFailure
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailFailure
import retrofit2.HttpException

/** */
internal fun HttpException.toMovieFailure(): GetAllMoviesFailure =
    when(HttpErrorCode.from(code())) {
        HttpErrorCode.HTTP_NOT_FOUND -> GetAllMoviesFailure.NoMoviesFound
        else -> GetAllMoviesFailure.ServerFailure(code(),message())
    }

/** */
internal fun HttpException.toMovieDetailFailure(): GetMovieDetailFailure =
    when(HttpErrorCode.from(code())) {
        HttpErrorCode.HTTP_NOT_FOUND -> GetMovieDetailFailure.NoMovieFound
        else -> GetMovieDetailFailure.ServerFailure(code(),message())
    }