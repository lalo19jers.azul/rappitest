package com.mx.movies.data.dataSource.local

import com.mx.datasource.local.entity.MovieDb
import com.mx.datasource.local.entity.MovieDetailDb

/** */
internal suspend fun MovieLocalDataSourceImpl.getAllMoviesFromDB(): List<MovieDb> =
    db.movieDao.getMoviesFromDb()

/** */
internal suspend fun MovieLocalDataSourceImpl.getMovieDetailFromDb(): MovieDetailDb =
    db.movieDetailDao.getMovieDetailFromDb()