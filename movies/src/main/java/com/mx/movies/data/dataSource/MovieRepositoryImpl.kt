package com.mx.movies.data.dataSource

import com.mx.datasource.local.entity.MovieDb
import com.mx.datasource.local.entity.MovieDetailDb
import com.mx.domain.Either
import com.mx.movies.domain.MovieRepository
import com.mx.movies.domain.entity.getMovieDetail.MovieDetailInfo
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesFailure
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesParams
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesResponse
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailFailure
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailParams
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailResponse
import com.mx.network.internetConnection.InternetConnectionRepository

internal class MovieRepositoryImpl(
    private val movieRemoteDataSource: MovieRemoteDataSource,
    private val movieLocalDataSource: MovieLocalDataSource,
    internetConnectionRepository: InternetConnectionRepository
) : MovieRepository, InternetConnectionRepository by internetConnectionRepository {


    /** */
    override suspend fun getMovieInfo(
        params: GetAllMoviesParams
    ): Either<GetAllMoviesFailure, GetAllMoviesResponse> =
        movieRemoteDataSource.getMovieInfo(params)


    /** */
    override suspend fun getMovieDetail(
        params: GetMovieDetailParams
    ): Either<GetMovieDetailFailure, GetMovieDetailResponse> =
        movieRemoteDataSource.getMovieDetail(params)

    /** */
    override suspend fun getMoviesFromDb(): Either<GetAllMoviesFailure, List<MovieDb>> =
        movieLocalDataSource.getMoviesFromDb()

    /** */
    override suspend fun getMovieDetailFromDb(): Either<GetMovieDetailFailure, MovieDetailDb> =
        movieLocalDataSource.getMovieDetailFromDB()
    /*
    How it should be works:
    if (isOnline)
            movieRemoteDataSource.getMovieInfo()
    else movieRemoteDataSource.getMovieInfo()
     */

    /**
     * NOTE: for some reason variable [isOnline] always returns false.
     * I don't have more time to check and fix this issue but code commented above
     * is an example that how [isOnline] help us to verify is we connect to remoteDataSource
     * or localDataSource.
     * And this way I decided not change [GetAllMoviesResponse] from (model entity) to
     * List<Movie> (db entity)
     */

}