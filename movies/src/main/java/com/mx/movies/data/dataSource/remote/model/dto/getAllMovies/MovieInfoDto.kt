package com.mx.movies.data.dataSource.remote.model.dto.getAllMovies

import com.mx.movies.domain.entity.getAllMovies.MovieInfo
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieInfoDto(
    @Json(name = "description") val description: String,
    @Json(name = "name") val name: String,
    @Json(name = "items") val movies: List<MoviesDto>

) {

    /** */
    fun toMovieInfo() =
        MovieInfo(
            description = description,
            name = name,
            movies = movies.map { it.toMovie() }
        )
}
