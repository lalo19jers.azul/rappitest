package com.mx.movies.data.dataSource.local

import com.mx.datasource.local.AppDatabase
import com.mx.datasource.local.entity.MovieDb
import com.mx.datasource.local.entity.MovieDetailDb
import com.mx.domain.Either
import com.mx.movies.data.dataSource.MovieLocalDataSource
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesFailure
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailFailure

internal class MovieLocalDataSourceImpl(
    val db: AppDatabase
): MovieLocalDataSource {

    /** */
    override suspend fun getMoviesFromDb(): Either<GetAllMoviesFailure, List<MovieDb>> =
        try {
            Either.Right(getAllMoviesFromDB())
        } catch (e: Exception) {
            val failure = when(e) {
                is NullPointerException -> GetAllMoviesFailure.NullPointerException
                else -> GetAllMoviesFailure.UnknownFailure(e.message)
            }
            Either.Left(failure)
        }

    /** */
    override suspend fun getMovieDetailFromDB(): Either<GetMovieDetailFailure, MovieDetailDb> =
        try {
            Either.Right(getMovieDetailFromDb())
        } catch (e: Exception) {
            val failure = when(e) {
                is NullPointerException -> GetMovieDetailFailure.NullPointerException
                else -> GetMovieDetailFailure.UnknownFailure(e.message)
            }
            Either.Left(failure)
        }
}