package com.mx.movies.data.dataSource.remote.model.dto.getAllMovies

import com.mx.datasource.local.entity.MovieDb
import com.mx.datasource.local.entity.dbMovie
import com.mx.movies.domain.entity.getAllMovies.Movie
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MoviesDto(
    @Json(name = "backdrop_path") val backdropPath: String,
    @Json(name = "title") val title: String,
    @Json(name = "id") val id: Int,
    @Json(name = "overview") val overview: String,
    @Json(name = "poster_path") val posterPath: String,
    @Json(name = "vote_average") val voteAverage: Float,
    @Json(name = "video") val video: Boolean
) {

    /** */
    fun toMovie() =
        Movie(
            backdropPath = backdropPath,
            title = title,
            id = id,
            overview = overview,
            posterPath = posterPath,
            voteAverage = voteAverage,
            video = video
        )

    /** */
    fun toMovieDb() : dbMovie =
        dbMovie(
            id = id,
            backdropPath = backdropPath,
            title = title,
            overview = overview,
            posterPath = posterPath,
            voteAverage = voteAverage,
            video = video
        )
}
