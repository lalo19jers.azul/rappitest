package com.mx.movies.data.dataSource

import com.mx.domain.Either
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesFailure
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesParams
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesResponse
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailFailure
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailParams
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailResponse

interface MovieRemoteDataSource {

    /** */
    suspend fun getMovieInfo(
        params: GetAllMoviesParams
    ): Either<GetAllMoviesFailure, GetAllMoviesResponse>

    /** */
    suspend fun getMovieDetail(
        params: GetMovieDetailParams
    ): Either<GetMovieDetailFailure, GetMovieDetailResponse>
}