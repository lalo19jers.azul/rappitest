package com.mx.movies.data.dataSource

import com.mx.datasource.local.entity.MovieDb
import com.mx.datasource.local.entity.MovieDetailDb
import com.mx.domain.Either
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesFailure
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailFailure

interface MovieLocalDataSource {
    /** */
    suspend fun getMoviesFromDb(): Either<GetAllMoviesFailure, List<MovieDb>>
    /** */
    suspend fun getMovieDetailFromDB(): Either<GetMovieDetailFailure, MovieDetailDb>
}