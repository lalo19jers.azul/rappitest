package com.mx.movies.data.dataSource.remote

import com.mx.movies.data.dataSource.remote.model.dto.getAllMovies.MovieInfoDto
import com.mx.movies.data.dataSource.remote.model.dto.getMovieDetail.MovieDetailInfoDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApiService {
    /** */
    @GET(URL.GET_ALL_MOVIES)
    suspend fun getAllMovies(
        @Path("list_id") listId: Int,
        @Query("api_key") apiKey: String,
        @Query("language") language: String
    ): Response<MovieInfoDto>

    /** */
    @GET(URL.GET_MOVIE_DETAIL)
    suspend fun getMovieDetail(
        @Path("movie_id") movieId: Int,
        @Query("api_key") apiKey: String,
        @Query("language") language: String
    ): Response<MovieDetailInfoDto>

    private object URL {
        const val GET_ALL_MOVIES = "list/{list_id}"
        const val GET_MOVIE_DETAIL = "movie/{movie_id}"
    }
}