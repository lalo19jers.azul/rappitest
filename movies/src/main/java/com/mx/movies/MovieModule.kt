package com.mx.movies

import com.mx.movies.data.dataSource.MovieLocalDataSource
import com.mx.movies.data.dataSource.MovieRemoteDataSource
import com.mx.movies.data.dataSource.MovieRepositoryImpl
import com.mx.movies.data.dataSource.local.MovieLocalDataSourceImpl
import com.mx.movies.data.dataSource.remote.MovieApiService
import com.mx.movies.data.dataSource.remote.MovieRemoteDataSourceImpl
import com.mx.movies.domain.MovieRepository
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesUseCase
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailUseCase
import com.mx.movies.presentation.getAllMovies.GetAllMovies
import com.mx.movies.presentation.getAllMovies.GetAllMoviesImpl
import com.mx.movies.presentation.getMovieDetail.GetMovieDetail
import com.mx.movies.presentation.getMovieDetail.GetMovieDetailImpl
import org.koin.dsl.module
import retrofit2.Retrofit

val moviesModule = module {

    /** PRESENTATION */
    single<GetAllMovies> { GetAllMoviesImpl(getAllMoviesUseCase = get()) }
    single<GetMovieDetail> { GetMovieDetailImpl(getMovieDetailUseCase = get()) }

    /** USE CASE */
    factory { GetAllMoviesUseCase(repository = get()) }
    factory { GetMovieDetailUseCase(repository = get()) }

    /** REPOSITORY */
    single<MovieRepository> { MovieRepositoryImpl(
        movieRemoteDataSource = get(),
        movieLocalDataSource = get(),
        internetConnectionRepository = get()
    ) }

    /** REMOTE DATA SOURCE */
    single<MovieRemoteDataSource> { MovieRemoteDataSourceImpl(
        apiService = get(),
        db = get()
    ) }

    /** LOCAL DATA SOURCE */
    single<MovieLocalDataSource> {
        MovieLocalDataSourceImpl(db = get())
    }

    /** API SERVICE */
    single { get<Retrofit>().create(MovieApiService::class.java) }
}