plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
}

android {

    compileSdkVersion(Sdk.COMPILE_SDK_VERSION)
    buildToolsVersion(Build.BUILD_TOOL_VERSION)

    defaultConfig {
        minSdkVersion(Sdk.MIN_SDK_VERSION)
        targetSdkVersion(Sdk.TARGET_SDK_VERSION)
        versionCode = AppCoordinates.APP_VERSION_CODE
        versionName = AppCoordinates.APP_VERSION_NAME

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            minifyEnabled(false)
            proguardFiles(
                    getDefaultProguardFile("proguard-android-optimize.txt"),
                    "proguard-rules.pro"
            )
        }
        create("staging") {
            versionNameSuffix = "staging"
        }
    }

}

dependencies {
    /** KOTLIN JETBRAINS */
    implementation(Dependencies.JET_BRAINS_KOTLIN)
    /** ANDROID X */
    implementation(Dependencies.CORE_KTX)
    implementation(Dependencies.APP_COMPAT)
    /** RETROFIT */
    implementation(Dependencies.RETROFIT)
    /** MOSHI */
    implementation(Dependencies.MOSHI)
    implementation(Dependencies.MOSHI_CONVERTER)
    kapt(Dependencies.MOSHI_CODEGEN)
    /** KOIN */
    implementation(Dependencies.KOIN_ANDROID)
    /** KOTLINX */
    implementation(KotlinX.KOTLINX_CONVERTER)
    implementation(KotlinX.KOTLINX)
    /** LIFECYCLE */
    implementation(Dependencies.LIFECYCLE_LIVE_DATA)
    /** COROUTINES */
    api(Dependencies.COROUTINES_CORE)
    /** MODULES */
    implementation(project(Module.DOMAIN))
    implementation(project(Module.NETWORK))
    implementation(project(Module.DATA_SOURCE))
    /** ROOM */
    implementation(Room.RUNTIME)
    implementation(Room.KTX_EXTENSIONS)
    kapt(Room.COMPILER)
    /** UNIT TESTS */
    testImplementation(TestingLib.JUNIT)
    testImplementation(AndroidTestingLib.MOCKK)
    testImplementation(Dependencies.COROUTINES_CORE)
    testImplementation(AndroidTestingLib.COROUTINES_TEST)
    testImplementation(AndroidTestingLib.MOCK_WEB_SERVER)

    /** INTEGRATION TESTS */
    androidTestImplementation(AndroidTestingLib.ANDROIDX_TEST_EXT_JUNIT)
}