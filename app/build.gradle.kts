plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-parcelize")
    id("kotlin-kapt")
    id("androidx.navigation.safeargs.kotlin")
}

android {
    compileSdkVersion(Sdk.COMPILE_SDK_VERSION)
    buildToolsVersion(Build.BUILD_TOOL_VERSION)


    defaultConfig {
        minSdkVersion(Sdk.MIN_SDK_VERSION)
        targetSdkVersion(Sdk.TARGET_SDK_VERSION)
        applicationId = AppCoordinates.APP_ID
        versionCode = AppCoordinates.APP_VERSION_CODE
        versionName = AppCoordinates.APP_VERSION_NAME
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }


    buildTypes {
        // Release production environment. JKS required.
        getByName("release") {
            minifyEnabled(false)
            proguardFiles(
                    getDefaultProguardFile("proguard-android-optimize.txt"),
                    "proguard-rules.pro"
            )

            resValue("string", "app_name_res", "Rappi")
        }

        // Debug environment
        getByName("debug") {
            resValue("string", "app_name_res", "Rappi Debug")
        }


    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    /** KOTLIN JETBRAINS */
    implementation(Dependencies.JET_BRAINS_KOTLIN)
    /** ANDROID X */
    implementation(Dependencies.CORE_KTX)
    implementation(Dependencies.APP_COMPAT)
    implementation(Dependencies.CONSTRAINT_LAYOUT)
    implementation(Dependencies.NAVIGATION_FRAGMENT)
    implementation(Dependencies.NAVIGATION_UI)
    implementation(Dependencies.LEGACY_SUPPORT)
    implementation(Dependencies.RECYCLERVIEW)
    /** DEBUG */
    implementation(Dependencies.TIMBER)
    /** GOOGLE */
    implementation(Dependencies.MATERIAL_COMPONENTS)
    /** KOTLINX */
    implementation(KotlinX.KOTLINX_CONVERTER)
    implementation(KotlinX.KOTLINX)
    /** KOIN */
    implementation(Dependencies.KOIN_ANDROID)
    implementation(Dependencies.KOIN_VIEWMODEL)
    /** LIFECYCLE */
    implementation(Dependencies.LIFECYCLE_LIVE_DATA)
    implementation(Dependencies.LIFECYCLE_EXTENSIONS)
    implementation(Dependencies.LIFECYCLE_VIEW_MODEL)
    /** RATING BAR */
    implementation(Dependencies.MATERIAL_STAR_BAR)
    /** MODULES */
    implementation(project(Module.DOMAIN))
    implementation(project(Module.NETWORK))
    implementation(project(Module.DATA_SOURCE))
    implementation(project(Module.MOVIES))
    /** COIL */
    implementation(Dependencies.COIL)
    /** APP INTRO */
    implementation(Dependencies.APP_INTRO)
    /** TESTING */
    testImplementation(TestingLib.JUNIT)
    androidTestImplementation(AndroidTestingLib.ANDROIDX_TEST_EXT_JUNIT)
    androidTestImplementation(AndroidTestingLib.ESPRESSO_CORE)
}