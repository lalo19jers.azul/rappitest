package com.mx.rappitest.common.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.core.content.ContextCompat
import coil.api.load
import coil.size.Scale
import com.mx.rappitest.R
import kotlin.random.Random

/** */
fun ImageView.loadImage(url: String?) {
    url?.let {
        if (it.isValidUrl())
            this.load(url) {
                error(R.drawable.bg_marvel)
                scale(Scale.FIT)
            }
        else load(R.drawable.marvel)
    }

}

/**
 * The images from API returns 404 not found. So this way
 * I use a random drawable from fill the list
 */
fun ImageView.setRandomImage(context: Context) {
    val list = listOf(
        ContextCompat.getDrawable(context, R.drawable.bg_marvel),
        ContextCompat.getDrawable(context, R.drawable.marvel),
        ContextCompat.getDrawable(context, R.drawable.spiderman),
        ContextCompat.getDrawable(context, R.drawable.marvel_two),
        ContextCompat.getDrawable(context, R.drawable.marvel_three)

    )
    val randomIndex = Random.nextInt(list.size);
    this.setImageDrawable(list[randomIndex])
}