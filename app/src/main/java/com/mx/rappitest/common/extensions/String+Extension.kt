package com.mx.rappitest.common.extensions

import android.util.Patterns

/** */
fun String.isValidUrl() =
    Patterns.WEB_URL.matcher(this).matches()
