package com.mx.rappitest.common.extensions

import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.mx.rappitest.R

/** */
fun Fragment.showProgressBar(messageRes: Int = R.string.default_loading_message) =
    requireContext().showProgressBar(messageRes)

/** */
fun Fragment.hideProgressBar() = requireContext().hideProgressBar()

fun View.presentShortSnackBar(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
}