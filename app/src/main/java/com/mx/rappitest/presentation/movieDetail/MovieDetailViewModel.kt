package com.mx.rappitest.presentation.movieDetail

import androidx.lifecycle.ViewModel
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesParams
import com.mx.movies.presentation.getMovieDetail.GetMovieDetail

class MovieDetailViewModel(
    getMovieDetail: GetMovieDetail
): ViewModel(), GetMovieDetail by getMovieDetail