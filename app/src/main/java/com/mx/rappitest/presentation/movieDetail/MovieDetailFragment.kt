package com.mx.rappitest.presentation.movieDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.mx.domain.Status
import com.mx.movies.domain.entity.getMovieDetail.MovieDetailInfo
import com.mx.movies.domain.useCase.getMovieDetail.GetMovieDetailParams
import com.mx.movies.presentation.getMovieDetail.GetMovieDetailStatus
import com.mx.rappitest.R
import com.mx.rappitest.common.extensions.hideProgressBar
import com.mx.rappitest.common.extensions.presentShortSnackBar
import com.mx.rappitest.common.extensions.setRandomImage
import com.mx.rappitest.common.extensions.showProgressBar
import com.mx.rappitest.databinding.MovieDetailFragmentBinding
import com.mx.rappitest.presentation.movieDetail.adapter.GenreAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class MovieDetailFragment : Fragment() {

    //region INIT COMPONENTS
    /* */
    private val binding: MovieDetailFragmentBinding
            by lazy { MovieDetailFragmentBinding.inflate(layoutInflater) }

    /* */
    private val viewModel: MovieDetailViewModel by viewModel()

    /* */
    private val args: MovieDetailFragmentArgs by navArgs()

    /* */
    private val genreAdapter: GenreAdapter
     by lazy { GenreAdapter() }
    //endregion

    //region LIFECYCLE
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getMovieDetail()
    }
    //endregion

    //region FUNCTIONS
    private fun getMovieDetail() {
        viewModel.getMovieDetailAsLiveData(
            GetMovieDetailParams(
                movieId = args.movie.id,
                language = "es-MX",
                apiKey = "21e1b83ab56b00bde4d649c973930d8b"
            )
        ).observe(
            viewLifecycleOwner, handleGetMovieDetailStatusObserver()
        )
    }

    private fun handleGetMovieDetailStatusObserver() = Observer<GetMovieDetailStatus> {
        requireContext().hideProgressBar()
        when (it) {
            is Status.Loading -> requireContext().showProgressBar()
            is Status.Error -> binding.root.presentShortSnackBar(it.failure.toString())
            is Status.Done -> setUpView(it.value.movieDetailInfo)
        }
    }

    private fun setUpView(movie: MovieDetailInfo) {
        binding.apply {
            val rating = (movie.voteAverage/2)
            tvDescription.text = movie.overview
            tvRuntime.text = if (movie.runtime > 0) {
                getString(R.string.runtime, movie.runtime.toString())
            } else {
                getString(R.string.no_runtime)
            }
            tvTagline.text = movie.tagline
            tvTitle.text = movie.title
            ivPoster.setRandomImage(requireContext())
            mtbRanking.rating = rating
            genreAdapter.submitList(movie.genres)
            rvGenres.adapter = genreAdapter
        }
    }
    //endregion

}