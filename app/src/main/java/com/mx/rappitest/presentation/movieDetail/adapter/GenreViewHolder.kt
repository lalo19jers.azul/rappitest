package com.mx.rappitest.presentation.movieDetail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mx.movies.domain.entity.getMovieDetail.Genre
import com.mx.rappitest.databinding.ItemGenreBinding

class GenreViewHolder(
    private val binding: ItemGenreBinding
) : RecyclerView.ViewHolder(binding.root) {

    /** */
    fun bind(genre: Genre) {
        binding.run {
            tvGenre.text = genre.name
        }
    }

    /**
     * Inflates item
     */
    companion object {
        fun from(parent: ViewGroup): GenreViewHolder {
            val layoutInflater = ItemGenreBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
            return GenreViewHolder(layoutInflater)
        }
    }

}