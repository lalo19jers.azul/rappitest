package com.mx.rappitest.presentation.home.adapter


import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.mx.movies.domain.entity.getAllMovies.Movie

class MovieAdapter(
    private val onRowClick: (Movie) -> Unit
) : ListAdapter<Movie, MovieViewHolder>(MovieDiffUtil()) {

    /** */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder =
        MovieViewHolder.from(parent)

    /** */
    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = getItem(position)
        holder.bind(movie =  movie, onRowClick = onRowClick)
    }

    /** */
    internal class MovieDiffUtil : DiffUtil.ItemCallback<Movie>() {
        /** */
        override fun areItemsTheSame(
            oldItem: Movie,
            newItem: Movie
        ): Boolean = oldItem == newItem

        /** */
        override fun areContentsTheSame(
            oldItem: Movie,
            newItem: Movie
        ): Boolean = oldItem == newItem

    }
}