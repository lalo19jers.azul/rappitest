package com.mx.rappitest.presentation.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mx.movies.domain.entity.getAllMovies.Movie
import com.mx.rappitest.common.extensions.setRandomImage
import com.mx.rappitest.databinding.ItemMovieBinding

class MovieViewHolder(
    private val binding: ItemMovieBinding
): RecyclerView.ViewHolder(binding.root) {

    /** */
    fun bind(movie: Movie, onRowClick: (Movie) -> Unit) {
        binding.run {
            ivPoster.setRandomImage(root.context)
            tvMovieTitle.text = movie.title
            tvOverview.text = movie.overview
            root.setOnClickListener { onRowClick(movie) }
        }
    }

    /**
     * Inflates item
     */
    companion object {
        fun from(parent: ViewGroup): MovieViewHolder {
            val layoutInflater = ItemMovieBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
            return MovieViewHolder(layoutInflater)
        }
    }

}