package com.mx.rappitest.presentation.movieDetail.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.mx.movies.domain.entity.getMovieDetail.Genre

class GenreAdapter() : ListAdapter<Genre, GenreViewHolder>(GenreDiffUtil()) {

    /** */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreViewHolder =
        GenreViewHolder.from(parent)

    /** */
    override fun onBindViewHolder(holder: GenreViewHolder, position: Int) {
        val genre = getItem(position)
        holder.bind(genre = genre)
    }

    /** */
    internal class GenreDiffUtil : DiffUtil.ItemCallback<Genre>() {
        /** */
        override fun areItemsTheSame(
            oldItem: Genre,
            newItem: Genre
        ): Boolean = oldItem == newItem

        /** */
        override fun areContentsTheSame(
            oldItem: Genre,
            newItem: Genre
        ): Boolean = oldItem == newItem

    }
}