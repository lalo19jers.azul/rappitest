package com.mx.rappitest.presentation.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mx.domain.Status
import com.mx.movies.domain.entity.getAllMovies.Movie
import com.mx.movies.presentation.getAllMovies.GetAllMoviesStatus
import com.mx.rappitest.common.extensions.hideProgressBar
import com.mx.rappitest.common.extensions.presentShortSnackBar
import com.mx.rappitest.common.extensions.showProgressBar
import com.mx.rappitest.databinding.HomeFragmentBinding
import com.mx.rappitest.presentation.home.adapter.MovieAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel


class HomeFragment : Fragment() {

    //region INIT COMPONENTS
    /* */
    private val binding: HomeFragmentBinding
            by lazy { HomeFragmentBinding.inflate(layoutInflater) }

    /* */
    private val viewModel: HomeViewModel by viewModel()

    /* */
    private val movieAdapter: MovieAdapter
            by lazy { MovieAdapter(onRowClick = onRowClick) }
    //endregion

    //region LIFECYCLE
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getMovies()
    }
    //endregion

    //region FUNCTIONS
    private fun getMovies() {
        viewModel.run {
            getAllMoviesAsLiveData(getMovieParams()).observe(
                viewLifecycleOwner, handleGetAllMoviesStatusObserver()
            )
        }
    }

    private fun handleGetAllMoviesStatusObserver() = Observer<GetAllMoviesStatus> {
        requireContext().hideProgressBar()
        when (it) {
            is Status.Loading -> requireContext().showProgressBar()
            is Status.Error -> binding.root.presentShortSnackBar(it.failure.toString())
            is Status.Done -> setUpView(movies = it.value.movies)
        }
    }
    //endregion

    //region SETUP VIEW

    /** */
    private fun setUpView(movies: List<Movie>) {
        binding.apply {
            movieAdapter.submitList(movies)
            rvMovies.adapter = movieAdapter
        }
    }

    /** */
    private val onRowClick: (Movie) -> Unit = {
        val directions = HomeFragmentDirections.actionHomeFragmentToMovieDetailFragment(movie = it)
        findNavController().navigate(directions)
    }

    //endregion

}