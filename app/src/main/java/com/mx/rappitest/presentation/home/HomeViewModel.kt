package com.mx.rappitest.presentation.home

import androidx.lifecycle.ViewModel
import com.mx.movies.domain.useCase.getAllMovies.GetAllMoviesParams
import com.mx.movies.presentation.getAllMovies.GetAllMovies

class HomeViewModel(
    getAllMovies: GetAllMovies
) : ViewModel(), GetAllMovies by getAllMovies {

    /** */
    fun getMovieParams(): GetAllMoviesParams =
        GetAllMoviesParams(page = 1, language = "es-MX", apiKey = "21e1b83ab56b00bde4d649c973930d8b")
}