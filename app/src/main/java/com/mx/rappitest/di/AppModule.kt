package com.mx.rappitest.di

import com.mx.datasource.httpClientModule
import com.mx.datasource.roomModule
import com.mx.movies.moviesModule
import com.mx.network.networkModule
import com.mx.rappitest.RappiApp
import com.mx.rappitest.di.presentation.homeModule
import com.mx.rappitest.di.presentation.movieDetailModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.core.module.Module

/**
 * This class is our dependency injection. Built with koin.
 * Our App architecture is Clean Architecture + MVVM.
 * And we change the Viewmodel factory with this dependency injection.
 * If you need more parameters will be welcome in this module.
 */
fun RappiApp.initKoin() {
    startKoin {
        val modules = getPresentationModules() + getSharedModules() + getFeatureModules()
        androidLogger(Level.ERROR)
        androidContext(applicationContext)
        modules(modules)
    }
}

/**
 *
 * @return [List]
 */
private fun getSharedModules(): List<Module> = listOf(
    /** **/
    networkModule,
    httpClientModule,
    roomModule
)

/**
 *
 * @return [List]
 */
private fun getFeatureModules(): List<Module> = listOf(
    moviesModule
)

/**
 * Presentation module is the layer that interacts with UI.
 * Presentation layer contains ViewModel, Fragments and Activities.
 * @return [List]
 */
private fun getPresentationModules(): List<Module> = listOf(
    /**  **/
    homeModule,
    movieDetailModule


)