package com.mx.rappitest.di.presentation

import com.mx.rappitest.presentation.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val homeModule = module {
    viewModel { HomeViewModel(getAllMovies = get()) }
}