package com.mx.rappitest.di.presentation

import com.mx.rappitest.presentation.movieDetail.MovieDetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val movieDetailModule = module {
    viewModel { MovieDetailViewModel(getMovieDetail = get()) }
}