package com.mx.rappitest

import android.app.Application
import com.mx.rappitest.di.initKoin
import timber.log.Timber

class RappiApp: Application() {

    /** */
    override fun onCreate() {
        super.onCreate()
        initKoin()
        Timber.plant(Timber.DebugTree())
    }
}