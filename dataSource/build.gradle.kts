plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
}

android {

    compileSdkVersion(Sdk.COMPILE_SDK_VERSION)
    buildToolsVersion(Build.BUILD_TOOL_VERSION)

    defaultConfig {
        minSdkVersion(Sdk.MIN_SDK_VERSION)
        targetSdkVersion(Sdk.TARGET_SDK_VERSION)
        versionCode = AppCoordinates.APP_VERSION_CODE
        versionName = AppCoordinates.APP_VERSION_NAME

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            minifyEnabled(false)
            proguardFiles(
                    getDefaultProguardFile("proguard-android-optimize.txt"),
                    "proguard-rules.pro"
            )
            buildConfigField(
                    "String",
                    "API_BASE_URL",
                    "\"https://api.themoviedb.org/3/\""
            )
        }
        getByName("debug") {
            buildConfigField(
                    "String",
                    "API_BASE_URL",
                    "\"https://api.themoviedb.org/3/\""
            )
        }

    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

}

dependencies {
    /** KOTLIN JETBRAINS */
    implementation(Dependencies.JET_BRAINS_KOTLIN)
    /** ANDROID X */
    implementation(Dependencies.CORE_KTX)
    implementation(Dependencies.APP_COMPAT)
    /** RETROFIT */
    implementation(Dependencies.RETROFIT)
    implementation(Dependencies.RETROFIT_OKHTTP)
    /** MOSHI */
    implementation(Dependencies.MOSHI_CONVERTER)
    implementation(Dependencies.MOSHI)
    implementation(Dependencies.MOSHI_REFLECTION)
    /** KOTLINX */
    implementation(KotlinX.KOTLINX_CONVERTER)
    implementation(KotlinX.KOTLINX)
    /** KOIN */
    implementation(Dependencies.KOIN_ANDROID)
    implementation(Dependencies.KOIN_VIEWMODEL)
    /** ROOM */
    implementation(Room.RUNTIME)
    implementation(Room.KTX_EXTENSIONS)
    kapt(Room.COMPILER)
    /** COROUTINES */
    api(Dependencies.COROUTINES_CORE)
    /** MODULES */
    implementation(project(Module.DOMAIN))
    /** TESTING */
    implementation(Dependencies.TIMBER)
    testImplementation(TestingLib.JUNIT)
    androidTestImplementation(AndroidTestingLib.ANDROIDX_TEST_EXT_JUNIT)
    androidTestImplementation(AndroidTestingLib.ESPRESSO_CORE)
}