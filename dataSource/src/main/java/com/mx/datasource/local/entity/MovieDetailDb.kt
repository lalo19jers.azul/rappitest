package com.mx.datasource.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

typealias dbMovieDetail = MovieDetailDb

@Entity(tableName = "movie_detail")
data class MovieDetailDb(
    @PrimaryKey val id: Int,
    val tagline: String,
    val title: String,
    val runtime: Int,
    val overview: String,
    @ColumnInfo(name = "poster_path") val posterPath: String,
    @ColumnInfo(name = "vote_average") val voteAverage: Float,
)