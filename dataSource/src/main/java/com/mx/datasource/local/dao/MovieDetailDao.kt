package com.mx.datasource.local.dao

import androidx.room.*
import com.mx.datasource.local.entity.MovieDetailDb

@Dao
interface MovieDetailDao {

    /** */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(movie: MovieDetailDb)

    /** */
    @Transaction
    @Query("SELECT * FROM movie_detail")
    suspend fun getMovieDetailFromDb(): MovieDetailDb
}