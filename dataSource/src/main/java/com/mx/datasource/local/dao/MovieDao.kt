package com.mx.datasource.local.dao


import androidx.room.*
import com.mx.datasource.local.entity.MovieDb

@Dao
interface MovieDao {

    /** */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(movie: MovieDb)

    /** */
    @Transaction
    @Query("SELECT * FROM movie")
    suspend fun getMoviesFromDb(): List<MovieDb>
}