package com.mx.datasource.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mx.datasource.local.dao.MovieDao
import com.mx.datasource.local.dao.MovieDetailDao
import com.mx.datasource.local.entity.MovieDb
import com.mx.datasource.local.entity.MovieDetailDb

@Database(
    entities = [
        MovieDb::class,
        MovieDetailDb::class
    ],
    version = 2,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    /* */
    abstract val movieDao: MovieDao

    /* */
    abstract val movieDetailDao: MovieDetailDao

    /** Creates Database instance */
    companion object {
        // Stores a strong reference to database object and this variable helps to
        // know if the reference exists or should be created.
        private var INSTANCE: AppDatabase? = null

        /**
         * Gets an ´AppDatabase´ instance.
         * First off, room builder is called and the context, reference class and database name
         * is provided, and builds the new instance.
         *
         * @param context Application context.
         */
        fun getInstance(context: Context): AppDatabase = INSTANCE ?: synchronized(this) {
            var instance = INSTANCE
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    "rappi_database"
                )
                    .fallbackToDestructiveMigration()
                    .build()

                INSTANCE = instance
            }
            instance
        }
    }
}