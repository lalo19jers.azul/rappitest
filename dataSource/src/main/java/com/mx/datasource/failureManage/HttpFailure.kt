package com.mx.datasource.failureManage

/** */
interface HttpFailure {

    /** Server failure code */
    val code: Int

    /** Server failure message */
    val message: String

}