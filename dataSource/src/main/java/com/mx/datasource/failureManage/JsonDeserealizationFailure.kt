package com.mx.datasource.failureManage

/** */
interface JsonDeserializationFailure {

    /** */
    val message: String?

}