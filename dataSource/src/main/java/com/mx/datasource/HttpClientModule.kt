package com.mx.datasource

import com.mx.datasource.remote.httpClient.HeaderInterceptor
import com.mx.datasource.remote.httpClient.HeaderInterceptorImpl
import com.mx.datasource.remote.httpClient.RetrofitBuilder
import org.koin.dsl.module

/* */
val httpClientModule = module {

    /** RETROFIT BUILDER */
    single {
        RetrofitBuilder(
            baseUrl = BuildConfig.API_BASE_URL,
            headerInterceptor = get()
        ).build()
    }

    /** HEADER INTERCEPTOR */
    single<HeaderInterceptor> {
        HeaderInterceptorImpl()
    }

}