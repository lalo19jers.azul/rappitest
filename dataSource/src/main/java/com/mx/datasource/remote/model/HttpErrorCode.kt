package com.mx.datasource.remote.model

/**
 * Handler server code error.
 */
enum class HttpErrorCode(val code: Int) {
    HTTP_BAD_REQUEST(400),
    HTTP_UNAUTHORIZED(401),
    HTTP_PAYMENT_REQUIRED(402),
    HTTP_FORBIDDEN(403),
    HTTP_NOT_FOUND(404),
    HTTP_BAD_METHOD(405),
    HTTP_NOT_ACCEPTABLE(406),
    HTTP_PROXY_AUTH(407),
    HTTP_CLIENT_TIMEOUT(408),
    HTTP_CONFLICT(409),
    HTTP_GONE(410),
    HTTP_LENGTH_REQUIRED(411),
    HTTP_PRECON_FAILED(412),
    HTTP_UNPROCESSABLE_ENTITY(422),
    HTTP_INTERNAL_ERROR(500);

    companion object {
        fun from(code: Int): HttpErrorCode {
            return when(code) {
                400 -> HTTP_BAD_REQUEST
                401 -> HTTP_UNAUTHORIZED
                402 -> HTTP_PAYMENT_REQUIRED
                403 -> HTTP_FORBIDDEN
                404 -> HTTP_NOT_FOUND
                405 -> HTTP_BAD_METHOD
                406 -> HTTP_NOT_ACCEPTABLE
                407 -> HTTP_PROXY_AUTH
                408 -> HTTP_CLIENT_TIMEOUT
                409 -> HTTP_CONFLICT
                410 -> HTTP_GONE
                411 -> HTTP_LENGTH_REQUIRED
                412 -> HTTP_PRECON_FAILED
                422 -> HTTP_UNPROCESSABLE_ENTITY
                else -> HTTP_INTERNAL_ERROR
            }
        }
    }
}