package com.mx.datasource.remote.httpClient


import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitBuilder(
    private val baseUrl: String,
    private val headerInterceptor: HeaderInterceptor? = null
) {

    /**
     *  Logging interceptor. By default, Retrofit doesn't log any request
     */
    private val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    /** */
    private val moshiConverterFactory = MoshiConverterFactory.create()

    /** */
    private val timeOut = 100L

    /**
     * Build Retrofit instance
     * @return [Retrofit]
     */
    fun build(): Retrofit =
        Retrofit.Builder().client(buildHttpClient())
            .baseUrl(baseUrl)
            .addConverterFactory(moshiConverterFactory)
            .build()

    /**
     * @return [OkHttpClient]
     */
    private fun buildHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(timeOut, TimeUnit.SECONDS)
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .addInterceptor(getHeaderInterceptor())
            .apply { addInterceptor(loggingInterceptor) }
            .build()

    /**
     * @return [Interceptor]
     */
    private fun getHeaderInterceptor() = Interceptor {
        val authorizationType = headerInterceptor?.getAuthorizationType()
        val accessToken = headerInterceptor?.getAuthorizationValue()
        if (accessToken == null) {
            it.proceed(it.request())
        } else {
            val newRequest = it.request().newBuilder()
                .addHeader("Authorization", "$authorizationType $accessToken")
                .build()
            it.proceed(newRequest)
        }
    }

}