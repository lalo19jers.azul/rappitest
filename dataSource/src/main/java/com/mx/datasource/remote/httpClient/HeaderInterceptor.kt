package com.mx.datasource.remote.httpClient

interface HeaderInterceptor {

    /**
     * This authorization type goes in the header (if interceptor exists).
     * e.g. JWT, Bearer, etc.
     *
     * @return Type in [String] format.
     */
    fun getAuthorizationType(): String

    /**
     * This value could be a token.
     * e.g. JWT value
     *
     * @return Value in [String] format.
     */
    fun getAuthorizationValue(): String?

}