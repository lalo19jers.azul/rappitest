package com.mx.datasource

import com.mx.datasource.local.AppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

/* */
val roomModule = module {

    /* */
    single { AppDatabase.getInstance(androidContext()) }

}