package com.mx.network

import com.mx.network.internetConnection.InternetConnectionApiService
import com.mx.network.internetConnection.InternetConnectionRepository
import com.mx.network.internetConnection.InternetConnectionRepositoryImpl
import com.mx.network.internetConnection.InternetConnectionRetrofitBuilder
import org.koin.dsl.module

/* */
val networkModule = module {

    /* Internet connection repository instance */
    single<InternetConnectionRepository>(createdAtStart = true) {
        InternetConnectionRepositoryImpl()
    }
    /* */
    single<InternetConnectionApiService> {
        InternetConnectionRetrofitBuilder()
            .build()
            .create(InternetConnectionApiService::class.java)
    }

}