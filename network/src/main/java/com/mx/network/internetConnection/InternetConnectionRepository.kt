package com.mx.network.internetConnection

import androidx.lifecycle.LiveData

interface InternetConnectionRepository  {
    /* */
    val isOnline: Boolean
    /* */
    val isOnLineLiveData: LiveData<Boolean>

    /** */
    suspend fun fetch()
}